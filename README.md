# Desafio Green Backend

## Motivos

- Para a autenticação por meio do Github funcionar corretamente, foi necessário a criação de uma aplicação Backend para que ele fique responsável pela obtenção do token de autenticação do Github, logo após o recebimento do `CODE` pelo Frontend.

## Guia de desenvolvimento (ir para a pasta /functions)

### Setup

- versão do node: `12.18.1`
- versão do yarn: `1.22.10`

- Instale as dependências usando: `yarn install`.

### Rodar localmente

- Execute o comando: `yarn start:dev`.

### Firebase functions

- O projeto foi configurado para ser disponibilizado por meio do firebase functions, para isso é necessário executar o comando (`firebase deploy`), para o projeto ser enviado.
- A API ficará disponível no link: [Desafio Green Back](https://us-central1-desafiogreen-7ad6f.cloudfunctions.net/api)

### Tecnologias

- `Express` e `Axios`
