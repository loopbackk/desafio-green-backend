const express = require('express');
const functions = require("firebase-functions");
const axios = require('axios');
const cors = require('cors')({origin: true});
const bodyParser = require('body-parser');
const app = express();

app.use(cors);

app.use(bodyParser());

app.post('/access_token', (req, res) => {

    const GITHUB_AUTH_ACCESSTOKEN_URL = "https://github.com/login/oauth/access_token";
    const CLIENT_ID = "9d2176931bb98e1fc22e";
    const CLIENT_SECRET = "84660bf3389ccafea873fd4aadee917da1f0ed70";
    const CODE = req.body.code;
  
    axios({
      method: 'post',
      url: GITHUB_AUTH_ACCESSTOKEN_URL,
      data: {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        code: CODE
      },
      headers:{
          'Content-Type': "application/json",
          "Accept": "application/json"
      }
    })
    .then((response)  =>  {
        console.log('Success ' + JSON.stringify(response.data));
        res.json({ data: response.data, status: response.status})
    })
    .catch((error) => {
      console.error('Error ' + error.message)
      res.json(error);
    })
  });

app.listen(5000, () => {
  console.log(`Example app listening at PORT :${process.env.PORT}`)
})  

exports.api = functions.https.onRequest(app);